#!/usr/bin/bash
RSYNC_OPTIONS=(-azh --stats --numeric-ids --sparse --delete -e 'ssh -i /root/.ssh/id_rsa' --log-file=/root/backup-ovh.log)

echo $(date)

echo "----------------------------DNS-------------------------------------"
mkdir -p /root/backup/dns/etc/
rsync "${RSYNC_OPTIONS[@]}" root@dns:/etc/named* /root/backup/dns/etc/
mkdir -p /root/backup/dns/var/named/
rsync "${RSYNC_OPTIONS[@]}" root@dns:/var/named/ /root/backup/dns/var/named

echo "--------------------------dockerhub-------------------------------------"
mkdir -p /root/backup/dockerhub/harbor
rsync "${RSYNC_OPTIONS[@]}" root@dockerhub:/root/harbor /root/backup/dockerhub/harbor
mkdir -p /root/backup/dockerhub/etc/docker
rsync "${RSYNC_OPTIONS[@]}" root@dockerhub:/etc/docker/ /root/backup/dockerhub/etc/docker

echo "---------------------------LDAP-------------------------------------"
mkdir -p /root/backup/ldap/var/www/html/
rsync "${RSYNC_OPTIONS[@]}" root@ldap:/var/www/html/ /root/backup/ldap/var/www/html/
mkdir -p /root/backup/ldap/etc/openldap/
rsync "${RSYNC_OPTIONS[@]}" root@ldap:/etc/openldap/ /root/backup/ldap/etc/openldap/
mkdir -p /root/backup/ldap/etc/httpd/
rsync "${RSYNC_OPTIONS[@]}" root@ldap:/etc/httpd/ /root/backup/ldap/etc/httpd/
mkdir -p /root/backup/ldap/etc/phpldapadmin/
rsync "${RSYNC_OPTIONS[@]}" root@ldap:/etc/phpldapadmin/ /root/backup/ldap/etc/phpldapadmin/
mkdir -p /root/backup/ldap/etc/sysconfig/
rsync "${RSYNC_OPTIONS[@]}" root@ldap:/etc/sysconfig/slapd /root/backup/ldap/etc/sysconfig/
mkdir -p /root/backup/ldap/etc/nginx/
rsync "${RSYNC_OPTIONS[@]}" root@ldap:/etc/nginx /root/backup/ldap/etc/nginx/

echo "--------------------------satis--------------------------------------"
mkdir -p /root/backup/satis/home/satis
rsync "${RSYNC_OPTIONS[@]}" root@satis:/home/satis /root/backup/satis/home/satis/
mkdir -p /root/backup/satis/var/www
rsync "${RSYNC_OPTIONS[@]}" -e 'ssh -i /root/.ssh/id_rsa' root@satis:/var/www/ /root/backup/satis/var/www/

echo "--------------------------testlink-------------------------------------"
mkdir -p /root/backup/testlink/etc/nginx/
rsync "${RSYNC_OPTIONS[@]}" root@testlink:/etc/nginx/ /root/backup/testlink/etc/nginx/
mkdir -p /root/backup/testlink/var/www
rsync "${RSYNC_OPTIONS[@]}" root@testlink:/var/www/ /root/backup/testlink/var/www/
mkdir -p /root/backup/testlink/etc/opt/remi/php71/
rsync "${RSYNC_OPTIONS[@]}" root@testlink:/etc/opt/remi/php71/ /root/backup/testlink/etc/opt/remi/php71/

ssh root@testlink "mysqldump --databases testlink" > /root/backup/testlink/testlink.sql

echo "--------------------------bamboo--------------------------------"
mkdir -p /root/backup/bamboo
rsync "${RSYNC_OPTIONS[@]}" root@bamboo:/home/bamboo/backups/ /root/backup/bamboo

echo "--------------------------jenkins--------------------------------"
mkdir -p /root/backup/jenkins
rsync "${RSYNC_OPTIONS[@]}" --exclude 'war/'  root@jenkins:/var/lib/docker/volumes/jenkins-data/_data/  /root/backup/jenkins

echo "--------------------------zabbix--------------------------------"
mkdir -p /root/backup/zabbix/etc/zabbix/
rsync "${RSYNC_OPTIONS[@]}" root@zabbix:/etc/zabbix/ /root/backup/zabbix/etc/zabbix/
mkdir -p /root/backup/zabbix/etc/httpd/
rsync "${RSYNC_OPTIONS[@]}" root@zabbix:/etc/httpd/ /root/backup/zabbix/etc/httpd/

echo "--------------------------proxy--------------------------------"
mkdir -p /root/backup/proxy/etc/nginx/
rsync "${RSYNC_OPTIONS[@]}" root@proxy:/etc/nginx/ /root/backup/proxy/etc/nginx/

echo "------------------------create tarball-------------------------------"
tar czf $(date +%Y%m%d).backup-ovh.tar.gz backup/

echo "------------------------push to s3-------------------------------"
aws s3 mv $(date +%Y%m%d).backup-ovh.tar.gz s3://rnd-backup-test/ --profile rnd-exchange-eu-west-2-backup-user --no-progress

echo "---------------------remove old backups-------------------------------"
for file in $(aws s3 ls s3://rnd-backup-test/ --profile rnd-exchange-eu-west-2-backup-user | awk '{print $4}'); do
  if [ "$file" \< "$(date -d '-14 days' +%Y%m%d)" ]; then
    echo "deleting $file"
    aws s3 rm s3://rnd-backup-test/$file --profile rnd-exchange-eu-west-2-backup-user
  fi
done

