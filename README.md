script fetches config from following hosts:
```
10.123.234.5 bamboo
10.123.234.7 dns
10.123.234.2 dockerhub
10.123.234.3 ldap
10.123.234.4 satis
10.123.234.6 testlink
10.123.234.17 jenkins
10.123.234.10 zabbix
10.123.234.12 proxy
```
Then create tarball and uploads it to s3. Bamboo is preparing own backup everyday at 00:00. Backup script is added to crontab at ansible host:
```
15 0 * * * /root/backup-ovh.sh >> /root/backup-ovh.log
```
After that, backups older than x days are removed.
